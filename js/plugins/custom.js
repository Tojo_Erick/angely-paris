$(document).ready(function () {
    "use strict";
    // <!-- Intializing Navigation Effect-->
    $('ul.navi-level-1 li').on('mouseenter', function () {
        $(this).children('ul.navi-level-2').addClass("open-navi-2 animated fadeInUp");
    });
    $('ul.navi-level-1 li').on('mouseleave', function () {
        $(this).children('ul.navi-level-2').removeClass("open-navi-2 animated fadeInUp");
    });





    // <!-- Intializing Navi Menu-->
    $("#mobile-menu").mobileMenu({
        MenuWidth: 250,
        SlideSpeed: 400,
        WindowsMaxWidth: 991,
        PagePush: false,
        FromLeft: false,
        Overlay: true,
        CollapseMenu: true,
        ClassName: "mobile-menu"
    });
    //Mobile Menu Scroll Enabel

    // --------------------------------------------------
    // Back To Top
    // --------------------------------------------------
    var offset = 450;
    var duration = 500;


    $(window).on('scroll', function () {
        if ($(this).scrollTop() > offset) {
            $('#to-the-top').fadeIn(duration);
        } else {
            $('#to-the-top').fadeOut(duration);
        }
    });

    $('#to-the-top').on('click', function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, duration);
        return false;
    });
    //Mobile Menu Scroll Enabel
    $(window).load(function () {
        $(".mCustomScrollbar").mCustomScrollbar(
            {
                scrollInertia: 400
            });

    });

    // wow jquery
    new WOW().init();
    // Skroll animate header
    var s = skrollr.init(
        {
            forceHeight: false,
            mobileCheck: function () {
                //hack - forces mobile version to be off
                return false;
            }

        }

    );
    // =====================================================
    if ($('.counter-home').length) {
        "use strict";
        $('.counter-home').counterUp({
            delay: 10,
            time: 2000
        });
    }
    if ($('.custom-select').length) {
        $(function () {
            $('.custom-select').fancySelect();
        });
    }
    if ($("#owl-variations").length) {
        var _owl = $("#owl-variations").owlCarousel({
            loop: true,
            autoPlay: false, //Set AutoPlay to 3 seconds
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 3],
            itemsMobile: [479, 2],
            pagination: true,
            navigation: true,
            margin: 20,
            autoHeight: true,
        });
        $("body").on("click",".variations .next-relate",function(event){
          _owl.trigger("owl.next");
        });
        $("body").on("click",".variations .prev-relate",function(event){
          _owl.trigger("owl.prev");
        });
    }
    if ($("#owl-variations-2").length) {
        var _owl2 = $("#owl-variations-2").owlCarousel({
            loop: true,
            autoPlay: false, //Set AutoPlay to 3 seconds
            items: 5,
            itemsDesktop: [1199, 5],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 3],
            itemsMobile: [479, 2],
            pagination: true,
            navigation: true,
            margin: 20,
            autoHeight: true,
        });
        $("body").on("click",".categories .next-relate",function(event){
          _owl2.trigger("owl.next");
        });
        $("body").on("click",".categories .prev-relate",function(event){
          _owl2.trigger("owl.prev");
        });
    }
});