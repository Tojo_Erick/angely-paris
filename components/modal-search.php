<div class="modal fade modal-search" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal">×</button>
    <div class="modal-dialog myModal-search">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <form role="search" method="get" class="search-form">
                    <input class="search-field" placeholder="Rechercher ici..." value="" title="" type="search">
                    <button type="submit" class="search-submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>