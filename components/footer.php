<footer class="footer-v1">
    <div class="footer-left">
        <a href="index.html">
            <img src="images/logo-footer.png " class="img-responsive" alt="Image">
        </a>
    </div>
    <!-- End Left Footer -->
    <nav>
        <ul>
            <li>
                <a class="text-cap" href="/">accueil</a>
            </li>
            <li>
                <a class="text-cap" href="/societe.php">lA SOCIété</a>
            </li>
            <li>
                <a class="text-cap" href="#">Politique de confidentialite</a>
            </li>
            <li>
                <a class="text-cap" href="/contact.php">Contact</a>
            </li>
        </ul>
    </nav>
    <!-- End Nav Footer -->
    <div class="footer-right">
        <ul class="social social-footer">
            <li class="facebook active"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
            <li class="instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
            <!-- <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
        </ul>
    </div>
    <!-- End Right Footer -->
</footer>
<!-- End Footer -->

<!-- <section class="copyright">
    <p>Copyright © 2016 Designed by <a href="#">AuThemes</a>. All rights reserved.</p>
</section> -->