<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Angely Paris | Contact</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/elegant-font.css">
    <!-- SCROLL BAR MOBILE MENU
  		================================================== -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Favicons
		  ================================================== -->
    <link rel="shortcut icon" href="favicon.png">
</head>

<body class="contact-page">
    <?php @include("./components/mobile-menu.php"); ?>
    <?php @include("./components/modal-search.php"); ?>
    <div id="page">
        <div id="skrollr-body">
            <?php @include("./components/header.php"); ?>
            <section>
                <div class="sub-header sub-header-1 sub-header-contact fake-position">
                    <div class="sub-header-content">
                        <h1 class="text-cap white-text h2">NOUS CONTACTEZ</h1>
                        <ol class="breadcrumb breadcrumb-arc text-cap">
                            <li>
                                <a href="/">ACCUEIL</a>
                            </li>
                            <li class="active">Contact</li>
                        </ol>
                    </div>
                </div>
            </section>
            <!-- End Section Sub Header -->
            <!-- Section form contact -->
            <section class="padding-top-30 padding-bot-90">
                <div class="container">
                    <div class="row">
                        <div class="contact-warp">
                            <div class="col-md-6 ">
                                <div class="left-contact">
                                    <h2 class="text-cap h3">UNE QUESTIONS ? CONTACTEZ-NOUS !</h2>
                                    <form class="form-inline form-contact-arc" name="contact" method="post" action="">
                                        <div class="row">
                                            <div class="form-group col-sm-12 ">
                                                <input type="text" class="form-control" name="yourName" id="yourName" placeholder="Votre nom">
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <input type="email" class="form-control" name="yourEmail" id="yourEmail" placeholder="Votre email">
                                            </div>
                                            <div class="form-group col-sm-12">
                                                <input type="text" class="form-control" name="yourPhone" id="phoneNumber" placeholder="Votre numéro de téléphone">
                                            </div>
                                        </div>
                                        <div class="input-content">
                                            <div class="form-group form-textarea">
                                                <textarea id="textarea" class="form-control" name="comments" rows="6" placeholder="Votre message"></textarea>
                                            </div>
                                        </div>
                                        <button class="ot-btn btn-main-color btn-long text-cap btn-submit" type="submit">Envoyer</button>
                                    </form> <!-- End Form -->
                                </div> <!-- End col -->
                            </div>
                            <div class="col-md-6 ">
                                <div class="right-contact">
                                    <h2 class="text-cap h3">INFO CONTACT</h2>
                                    <p>
                                    Une question ou besoin d’une info ? Nous faisons le maximum pour vous répondre dans les meilleurs délais.
                                    </p>
                                    <ul class="address">
                                        <li>
                                            <p><i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp; 56 Rue de la Fontaine 77240 Cesson</p>
                                        </li>
                                        <li>
                                            <p><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp; <a href="tel:0160569803">01 60 56 98 03</a></p>
                                        </li>
                                        <li>
                                            <p><i class="fa fa-envelope-o" aria-hidden="true"></i>&nbsp;&nbsp; <a href="mailto:contact@angely-paris.com">contact@angely-paris.com</a></p>
                                        </li>
                                    </ul>
                                </div>
                            </div> <!-- End col -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Section -->
            <!-- Section Google Map -->
            <div class="no-padding ">
                <div id="map-canvas" class=""></div>
            </div>
            <!-- End Section -->


            <?php @include("./components/footer.php"); ?>
        </div>
    </div>
    <!-- End page -->

    <a id="to-the-top"><i class="fa fa-angle-up"></i></a>
    <!-- Back To Top -->
    <!-- SCRIPT -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/plugins/wow.min.js"></script>
    <script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
    <!-- Initializing Google Maps -->
    <script src='https://maps.googleapis.com/maps/api/js?key=&sensor=false&extension=.js'></script>
    <script src="js/plugins/contact.js"></script>

    <?php @include("./components/footer-script.php"); ?>
</body>

</html>