<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Angely Paris | Romeo</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/elegant-font.css">
    <link rel="stylesheet" href="css/linearicons.css">
    <!-- SCROLL BAR MOBILE MENU
  		================================================== -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
    <!-- Fancy Select
  		================================================== -->

    <link rel="stylesheet" href="css/fancySelect.css" />
    <!-- OWL CAROUSEL
	  	================================================== -->
    <link rel="stylesheet" href="css/owl.carousel.css">

    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Favicons
		  ================================================== -->
    <link rel="shortcut icon" href="favicon.png">
</head>

<body class="shop single-product">
    <?php @include("./components/mobile-menu.php"); ?>
    <?php @include("./components/modal-search.php"); ?>
    <!-- End Modal Search-->
    <div id="page">
        <div id="skrollr-body">
            <?php @include("./components/header.php"); ?>
            <section>
                <div class="sub-header sub-header-1 sub-header-about fake-position">
                    <div class="sub-header-content">
                        <h1 class="text-cap white-text h2">ROMEO</h1>
                        <ol class="breadcrumb breadcrumb-arc text-cap">
                            <li>
                                <a href="/">ACCUEIL</a>
                            </li>
                            <li><a href="/tissus-d-ameublement.php">TISSUS D'AMEUBLEMENT</a></li>
                            <li class="active">Romeo</li>
                        </ol>
                    </div>
                </div>
            </section>
            <!-- End Section Sub Header -->
            <Section class="">
                <div class="container">
                    <form class="form-inline form-search-home-6 form-search-shop">
                        <div class="form-group">
                            <div class="input-group">
                                <button type="submit" class="btn-search-home-6 text-cap"><span aria-hidden="true" class="icon_search"></span></button>
                                <input class="form-control" id="exampleInputAmount" placeholder="RECHECHER" type="text">
                            </div>
                        </div>
                    </form>
                    <div class="blogFilter blog-terms line-effect-2">
                        <a href="#" data-filter="*" class="current text-cap">
                            <h2>Tout</h2>
                        </a>
                        <a href="#" data-filter=".unis" class="text-cap">
                            <h2>les unis</h2>
                        </a>
                        <a href="#" data-filter=".contemporains" class="text-cap">
                            <h2>les contemporains</h2>
                        </a>
                        <a href="#" data-filter=".classiques" class="text-cap">
                            <h2>les classiques</h2>
                        </a>
                        <a href="#" data-filter=".florabotanica" class="text-cap">
                            <h2>collection florabotanica</h2>
                        </a>
                        <a href="#" data-filter=".marnay" class="text-cap">
                            <h2>collection marnay</h2>
                        </a>
                        <a href="#" data-filter=".broadway" class="text-cap">
                            <h2>collection broadway</h2>
                        </a>
                    </div> <!-- End Project Fillter -->
                    <div class="arc-sorting"></div>
                </div><!-- End container -->
            </Section> <!--End List Product -->
            <Section class="padding product-details">
                <div class="container">
                    <div class="row">
                        <div class="detail-product-warp">
                            <div class="col-md-6 img-case">
                                <div class="product-detail-image">
                                    <img alt="product-name" src="/images/Shop/product-1.png">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="single-product-detail">
                                    <h3 class="text-cap product-title">Romeo</h3>
                                    <!-- <p class="price">
                                        <span class="old-price">$399.00</span> $299.00
                                    </p> -->
                                    <div class="divider-line"></div>
                                    <p class="product-description">Velours qualité siège, rideau et accessoire. Résistance de 30.000 tours. 100% Polyester. 140 cm de large. Disponible en 19 couleurs.</p>
                                    <!-- <div class="product-data">
                                        <div class="color-choose">
                                            <span class="text-cap clearfix">Color:</span>
                                            <a href="#">
                                                <img src="http://placehold.it/32x32/ccc.jpg" class="img-responsive" alt="Image">
                                            </a>
                                            <a href="#">
                                                <img src="http://placehold.it/32x32/ccc.jpg" class="img-responsive" alt="Image">
                                            </a>
                                            <a href="#">
                                                <img src="http://placehold.it/32x32/ccc.jpg" class="img-responsive" alt="Image">
                                            </a>
                                        </div>
                                        <div class="size-choose">
                                            <span class="text-cap clearfix">Size:</span>
                                            <a href="#">
                                                XS
                                            </a>
                                            <a href="#">
                                                S
                                            </a>
                                            <a href="#" class="active">
                                                M
                                            </a>
                                            <a href="#">
                                                L
                                            </a>
                                            <a href="#">
                                                XL
                                            </a>
                                        </div>
                                        <div class="quantity-choose">
                                            <input type="number" size="4" class="input-text qty text" title="Qty" value="1" name="quantity" min="1" step="1">
                                            <a href="#" class="ot-btn btn-main-color ">Add to Cart</a>
                                        </div>
                                    </div> -->
                                    <p class="product-code">
                                        <span class="text-cap">Référence :</span>
                                        rom1 - rom19
                                    </p>
                                    <!-- <p class="product-tag">
                                        <span class="text-cap">SKU :</span>
                                        <a href="#">table,</a>
                                        <a href="#">wood,</a>
                                        <a href="#">interior design,</a>
                                        <a href="#">furniture</a>
                                    </p> -->

                                    <div class="accrodion-warp">
                                        <div class="panel-group accrodion-style-1" id="accordion" role="tablist" aria-multiselectable="true">

                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                            <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                                                            Description
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                            <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                                                            Additional Information
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch.
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                            <!-- <i class="more-less glyphicon glyphicon-plus"></i> -->
                                                            Reviews (2)
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                                    </div>
                                                </div>
                                            </div>

                                        </div><!-- panel-group -->
                                    </div>
                                </div>
                            </div>
                        </div><!-- End Product Warp -->
                    </div><!-- End Row -->
                </div><!-- End container -->
            </Section> <!--End List Product -->

            <section class="variations">
                <div class="container">
                    <div class="col-12">
                        <h2 class="title text-cap">COLORIS DISPONIBLES :</h2>
                        <div class="owl-relate-warp">
                            <div class="customNavigation">
                                <a class="btn prev-relate"><i class="fa fa-angle-left"></i></a>
                                <a class="btn next-relate"><i class="fa fa-angle-right"></i></a>
                            </div><!-- End owl button -->

                            <div id="owl-variations" class="owl-carousel owl-theme owl-relate clearfix">
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-1.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/01 SILVER 
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-2.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/16 saffron
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-3.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/15 CLARET
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-4.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/14 OBSIDIAN
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-5.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/11 petrol
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/variation-1.jfif" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Réf : ROM/01 SILVER 
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End row partners -->
                    </div>
                </div>
            </section>

            <section class="categories">
                <div class="container">
                    <div class="col-12">
                        <h2 class="title text-cap">DANS LA MêME Catégorie</h2>
                        <div class="owl-relate-warp">
                            <div class="customNavigation">
                                <a class="btn prev-relate"><i class="fa fa-angle-left"></i></a>
                                <a class="btn next-relate"><i class="fa fa-angle-right"></i></a>
                            </div><!-- End owl button -->

                            <div id="owl-variations-2" class="owl-carousel owl-theme owl-relate clearfix">
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-12.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                Eragny 
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-3.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                panther
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-8.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                velluti
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-6.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                puma
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-7.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                oxford
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="variation-item">
                                        <a href="#" class="variations-warp">
                                            <img src="images/Shop/product-9.png" class="img-responsive" alt="Image">
                                        </a>
                                        <div class="variation-info">
                                            <a href="#">
                                                <h3 class="text-cap">
                                                SAMBOURG LARGE
                                                </h3>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End row partners -->
                    </div>
                </div>
            </section>
            <!-- End Section subcribe -->
            <?php @include("./components/footer.php"); ?>
        </div>
    </div>
    <!-- End page -->

    <a id="to-the-top"><i class="fa fa-angle-up"></i></a>
    <!-- Back To Top -->
    <!-- SCRIPT -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins/fancySelect.js"></script>
    <script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
    <script src="js/plugins/wow.min.js"></script>
    <!-- Initializing Owl Carousel
	    ================================================== -->
    <script src="js/plugins/owl.carousel.js"></script>
    <script src="js/plugins/custom-owl.js"></script>

    <?php @include("./components/footer-script.php"); ?>

</body>

</html>