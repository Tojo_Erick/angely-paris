 <!-- Mobile Menu
    ================================================== --> 
    <script src="js/plugins/jquery.mobile-menu.js"></script>  

    <!-- PreLoad
	    ================================================== -->
<script type="text/javascript" src="js/plugins/royal_preloader.min.js"></script>
<script type="text/javascript">
    (function($) {
        "use strict";
        Royal_Preloader.config({
            mode: 'logo',
            logo: 'images/logo.png',
            timeout: 1,
            showInfo: false,
            opacity: 1,
            background: ['#FFFFFF']
        });
    })(jQuery);
</script>

<!-- Global Js
	    ================================================== -->
<script src="js/plugins/custom.js"></script>
