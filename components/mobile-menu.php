<div class="mobile-menu-first">
    <div id="mobile-menu" class="mobile-menu">
        <div class="header-mobile-menu">
            <!-- <a href="tel:0160569803">01 60 56 98 03</a> -->
            <div class="mm-toggle">
                <span aria-hidden="true" class="icon_close"></span>
            </div>
        </div> <!-- Mobile Menu -->
        <div class="mCustomScrollbar light" data-mcs-theme="minimal-dark">

            <ul>
                <li><a href="/"><i class="icon-home"></i> Accueil</a></li>
                <li><a href="/societe.php"><span>La société</span></a></li>
                <li><a href="/tissus-d-ameublement.php"><span>Tissus d'ameublement</span></a></li>
                <li><a href="/mobilier-sur-mesure.php"><span>Mobilier sur mesure </span></a></li>

                <li><a href="/nos-distributions.php"><span>Nos distributions</span></a></li>
                <li><a href="#"><span>catalogue</span></a></li>
                <li><a href="/contact.php"><span>Contact</span></a></li>
                <!-- <li><a href="contact.html"><span>Contact</span></a>
                    <ul>
                        <li><a href="contact.html">Contact Style 1</a></li>
                        <li><a href="contact_2.html">Contact Style 2</a></li>
                        <li><a href="contact_3.html">Contact Style 3</a></li>
                    </ul>
                </li> -->
            </ul>
            <div class="footer-mobile-menu">
                <ul class="social">
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    <li class="instagram"><a href="#"><i class="fa fa-instagram"></i></a></li>
                    <!-- <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                </ul>

                <ul class="address-footer-mobile">
                    <li>
                        <p> 56 Rue de la Fontaine, 77240 Cesson</p>
                    </li>
                    <li>
                        <p><a href="tel:0160569803">01 60 56 98 03 </a></p>
                    </li>
                    <li>
                        <p> <a href="mailto:contact@angely-paris.com">contact@angely-paris.com</a></p>
                    </li>

                </ul>
            </div>
        </div> <!-- /#rmm   -->
    </div>
</div><!-- End Mobile Menu -->