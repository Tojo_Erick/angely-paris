<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Angely Paris | Accueil</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Font -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/elegant-font.css">

	<!-- REVOLUTION STYLE SHEETS -->
	<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
	<!-- REVOLUTION LAYERS STYLES -->
	<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
	<!-- REVOLUTION NAVIGATION STYLES -->
	<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
	<!-- OWL CAROUSEL
	  	================================================== -->
	<link rel="stylesheet" href="css/owl.carousel.css">
	<!-- SCROLL BAR MOBILE MENU
  		================================================== -->
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">

	<!-- Favicons
		  ================================================== -->
	<link rel="shortcut icon" href="favicon.png">
</head>

<body class="home">
	<?php @include("./components/mobile-menu.php"); ?>
	<?php @include("./components/modal-search.php"); ?>
	<!-- End Modal Search-->
	<div id="page">
		<div id="skrollr-body">
			<?php @include("./components/header.php"); ?>
			<h1>Angely Paris</h1>
			<section class="section-banner">
				<div class="rev_slider_wrapper">
					<!-- START REVOLUTION SLIDER 5.0 auto mode -->
					<div id="slider-h1" class="rev_slider slider-home-1" data-version="5.0">
						<ul>
							<!-- SLIDE  -->
							<li data-transition="parallaxtoright" data-masterspeed="1000">

								<!-- MAIN IMAGE -->
								<img src="images/home-banner.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10">

								<!-- LAYER NR. 1 -->
								<div class="tp-caption heading-2 white-text" data-x="center" data-y="center" data-voffset="-80" data-transform_in="y:-80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:-80px;opacity:0;s:300;" data-start="1000">CONSULTEZ NOTRE CATALOGUE EN LIGNE
								</div>
								<!-- LAYER NR. 2 -->
								<div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center" data-transform_in="y:80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:80px;opacity:0;s:300;" data-start="1400">Découvrez notre gamme<br> de mobilier sur-mesure
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/mobilier-sur-mesure.php" class="ot-btn btn-main-color text-cap ">Nos produits</a>

								</div>
								<!-- LAYER NR. 4 -->
								<div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/contact.php" class="ot-btn btn-sub-color text-cap  ">En savoir plus</a>
								</div>

							</li>
							<li data-transition="parallaxtoright" data-masterspeed="1000">

								<!-- MAIN IMAGE -->
								<img src="images/home-banner.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10">

								<!-- LAYER NR. 1 -->
								<div class="tp-caption heading-2 white-text" data-x="center" data-y="center" data-voffset="-80" data-transform_in="y:-80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:-80px;opacity:0;s:300;" data-start="1000">CONSULTEZ NOTRE CATALOGUE EN LIGNE
								</div>
								<!-- LAYER NR. 2 -->
								<div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center" data-transform_in="y:80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:80px;opacity:0;s:300;" data-start="1400">Découvrez notre gamme<br> de mobilier sur-mesure
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/mobilier-sur-mesure.php" class="ot-btn btn-main-color text-cap ">Nos produits</a>

								</div>
								<!-- LAYER NR. 4 -->
								<div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/contact.php" class="ot-btn btn-sub-color text-cap  ">En savoir plus</a>
								</div>

							</li>
							<!-- SLIDE  -->
							<li data-transition="parallaxtoright" data-masterspeed="1000">

								<!-- MAIN IMAGE -->
								<img src="images/home-banner.png" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10">

								<!-- LAYER NR. 1 -->
								<div class="tp-caption heading-2 white-text" data-x="center" data-y="center" data-voffset="-80" data-transform_in="y:-80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:-80px;opacity:0;s:300;" data-start="1000">CONSULTEZ NOTRE CATALOGUE EN LIGNE
								</div>
								<!-- LAYER NR. 2 -->
								<div class="tp-caption heading-1 white-text text-cap " data-x="center" data-y="center" data-transform_in="y:80px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:80px;opacity:0;s:300;" data-start="1400">Découvrez notre gamme<br> de mobilier sur-mesure
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption btn-1" data-x="center" data-hoffset="-85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/mobilier-sur-mesure.php" class="ot-btn btn-main-color text-cap ">Nos produits</a>

								</div>
								<!-- LAYER NR. 4 -->
								<div class="tp-caption btn-2" data-x="center" data-hoffset="85" data-y="center" data-voffset="100" data-transform_in="y:100px;opacity:0;s:800;e:easeInOutCubic;" data-transform_out="y:200px;opacity:0;s:300;" data-start="1600">
									<a href="/contact.php" class="ot-btn btn-sub-color text-cap  ">En savoir plus</a>
								</div>

							</li>

						</ul>
					</div><!-- END REVOLUTION SLIDER -->
				</div><!-- END REVOLUTION SLIDER WRAPPER -->
			</section>
			<!-- End Section Slider -->

			<section class="padding societe">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<h2 class="title text-cap">lA Société</h2>
							<div class="divider divider-1">
								<svg class="svg-triangle-icon-container">
									<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
								</svg>
							</div>
						</div>
						<!-- End Title -->
						<div class="row">
							<div class="col-md-6">
								<div class="block-img-right">
									<div class="text-box">
										<h3 class="text-cap h4"><mark>Angely</mark> </h3>
										<p>
											Crée en 1998 par Pierre Richard Calice, la société Angely Paris est éditeur et distributeur de tissus d'ameublement haut de gamme pour la décoration d'intérieure.
											<br /><br />La société collabore de manière significative avec les Tapissiers, Décorateurs indépendants et Bureaux d'études pour répondre à leurs besoins en proposant à ces professionnels du secteur une large offre de textiles alliant différents styles et matières grâce notamment à la diversité des marques que nous distribuons.
											<br /><br /><b>Une remarque ou une question ? <a href="contact.html" style="color:#666;">Contactez-nous !</a></b>
										</p>
										<a href="/societe.php" class="ot-btn btn-main-color text-cap lp">Lire plus</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="block-img-left">
									<div class="img-block"><img src="images/la-societe.jfif" class="img-responsive" alt="Image"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Section What we do -->

			<section class="padding tissus dark-section">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<h2 class="title text-cap">Tissus d'ameublement</h2>
							<div class="divider divider-1">
								<svg class="svg-triangle-icon-container">
									<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
								</svg>
							</div>
						</div>
						<!-- End Title -->
					</div>
			</section>

			<section class="promos">
				<div class="promotion-box">
					<figure class="effect-layla">
						<img src="images/promo-home-1.png" alt="grace" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">gRACE</h3>
								<p>Les Unis</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
					<figure class="effect-layla">
						<img src="images/promo-home-2.png" alt="OXFORD" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">OXFORD</h3>
								<p>Les Unis</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
					<figure class="effect-layla">
						<img src="images/promo-home-3.png" alt="PUMBA" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">PUMBA</h3>
								<p>Les Unis</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
					<figure class="effect-layla">
						<img src="images/promo-home-4.jpg" alt="MARNAY" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">MARNAY</h3>
								<p>Collection Marnay</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
					<figure class="effect-layla">
						<img src="images/promo-home-5.jfif" alt="VELVET" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">VELVET</h3>
								<p>Les Unis</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
					<figure class="effect-layla">
						<img src="images/promo-home-6.png" alt="Preston" />
						<figcaption>
							<div class="flt">
								<h3 class="text-cap white-text">Preston</h3>
								<p>Collection Florabotanica</p>
							</div>
							<div class="flb">
								<a href="/shop_single.php" class="ot-btn btn-main-color text-cap">Voir plus</a>
							</div>
						</figcaption>
					</figure>
				</div>
			</section>
			<!-- End Section Promotion -->

			<section class="padding">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<h2 class="title text-cap ">Nos statistiques</h2>
							<div class="divider divider-1">
								<svg class="svg-triangle-icon-container">
									<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
								</svg>
							</div>
						</div>
						<!-- End Title -->
					</div>
				</div>
			</section>

			<section class="padding stats">
				<div class="container">
					<div class="row">
						<div class="row">
							<div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay=".25s">
								<div class="block-img-full">
									<div class="wrp">
										<div class="wrp2">
											<a class="img-block" href="#"><img src="images/stats-1.png" class="img-responsive" alt="Image"></a>
											<div class="text-box">
												<h3 class="text-cap">Tissus pour meuble</h3>
												<p>
													<span class="counter-home">52</span>
												</p>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay=".5s">
								<div class="block-img-full">
									<div class="wrp">
										<div class="wrp2">
											<a class="img-block" href="#"><img src="images/stats-2.png" class="img-responsive" alt="Image"></a>
											<div class="text-box">
												<h3 class="text-cap">Clients fidèles</h3>
												<p>
													<span class="counter-home">788</span>+
												</p>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay=".75s">
								<div class="block-img-full">
									<div class="wrp">
										<div class="wrp2">
											<a class="img-block" href="#"><img src="images/stats-3.png" class="img-responsive" alt="Image"></a>
											<div class="text-box">
												<h3 class="text-cap">Gamme de meubles</h3>
												<p>
													<span class="counter-home">12</span>+
												</p>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="col-sm-6 col-md-3 wow fadeInRight" data-wow-delay="1s">
								<div class="block-img-full">
									<div class="wrp">
										<div class="wrp2">
											<a class="img-block" href="#"><img src="images/stats-4.png" class="img-responsive" alt="Image"></a>
											<div class="text-box">
												<h3 class="text-cap">Professionnels</h3>
												<p>
													<span class="counter-home">8</span>
												</p>
											</div>
										</div>
									</div>

								</div>
							</div>
							<!-- End Col -->
						</div>
						<!-- End Row -->
					</div>
				</div>
			</section>

			<section class="padding clearfix fixbug-inline-block">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<div class="title-block">
								<h2 class="title text-cap">Tout pour votre maison</h2>
								<div class="divider divider-1">
									<svg class="svg-triangle-icon-container">
										<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
									</svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Title -->
			<section class="no-padding clearfix fixbug-inline-block pour-la-maison">
				<div class="container">
					<div class="row">
						<div class="chooseus-container text-center">
							<div class="chooseus-item">
								<h3 class="text-cap h4">canapés</h3>
								<div class="chooseus-canvas-item">
									<svg class="svg-hexagon">
										<polygon class="hexagon" points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
									</svg>
									<!-- End Hexagon -->
									<svg class="svg-triangle-dotted">
										<polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
									</svg>
									<!-- End Triangle Dotted -->
									<div class="triangle-img-warp tri">
										<img src="images/tout-1.png" class="img-responsive" alt="Image">
									</div>
								</div>
							</div>

							<!-- End -->

							<div class="chooseus-item">
								<a href="#">
									<h3 class="text-cap h4">Têtes de lit</h3>
								</a>
								<div class="chooseus-canvas-item">
									<svg class="svg-hexagon">
										<polygon class="hexagon" points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
									</svg>
									<!-- End Hexagon -->
									<svg class="svg-triangle-dotted svg-tri-2">
										<polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
									</svg>
									<!-- End Triangle Dotted -->
									<div class="triangle-img-warp tri2">
										<img src="images/tout-2.png" class="img-responsive" alt="Image">
									</div>
								</div>
							</div>

							<!-- End -->

							<div class="chooseus-item">
								<a href="#">
									<h3 class="text-cap h4">fauteuils</h3>
								</a>
								<div class="chooseus-canvas-item">
									<svg class="svg-hexagon">
										<polygon class="hexagon" points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
									</svg>
									<!-- End Hexagon -->
									<svg class="svg-triangle-dotted svg-tri-3">
										<polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
									</svg>
									<!-- End Triangle Dotted -->
									<div class="triangle-img-warp tri3">
										<img src="images/tout-3.png" class="img-responsive" alt="Image">
									</div>
								</div>
							</div>

							<!-- End -->

							<div class="chooseus-item">
								<a href="#">
									<h3 class="text-cap h4">Chaises</h3>
								</a>
								<div class="chooseus-canvas-item">
									<svg class="svg-hexagon">
										<polygon class="hexagon" points="285 100,285 250,155 325,25 250,25 100,155 25"></polygon>
									</svg>
									<!-- End Hexagon -->
									<svg class="svg-triangle-dotted svg-tri-4">
										<polygon class="triangle-div" points="2 220,254 220,128 0"></polygon>
									</svg>
									<!-- End Triangle Dotted -->
									<div class="triangle-img-warp tri4">
										<img src="images/tout-4.png" class="img-responsive" alt="Image">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Section Why Choose Us -->

			<section class="padding dist-1">
				<div class="title-block">
					<h2 class="title text-cap">Nos DISTRIBUTIONS</h2>
					<div class="divider divider-1">
						<svg class="svg-triangle-icon-container">
							<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
						</svg>
					</div>
				</div>
			</section>
			<!-- End Title -->
			<section class="no-padding distributions">
				<div class="lastest-project-warp clearfix">

					<div class="clearfix projectContainer">

						<div class="element-item  Residential">

							<img src="images/dist-1.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap text-cap">ROMO</h3>
								</a>
							</div>
						</div>

						<div class="element-item Residential ">

							<img src="images/dist-2.jfif" class="img-responsive" alt="Image">

							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">Kirkby design</h3>
								</a>
							</div>
						</div>

						<div class="element-item Ecommercial">
							<img src="images/dist-3.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">BLACK EDITION</h3>
								</a>
							</div>
						</div>

						<div class="element-item Ecommercial ">
							<img src="images/dist-4.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">MARK Alexender</h3>
								</a>
							</div>
						</div>

						<div class="element-item Office">
							<img src="images/dist-5.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">VILLA NOVA</h3>
								</a>
							</div>
						</div>

						<div class="element-item Office">
							<img src="images/dist-6.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">Zinc textilles</h3>
								</a>
							</div>
						</div>

						<div class="element-item Hospital ">
							<img src="images/dist-7.png" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">HEATHFIELD & CO</h3>
								</a>
							</div>
						</div>

						<div class="element-item Hospital">

							<img src="images/dist-8.jfif" class="img-responsive" alt="Image">
							<div class="project-info">
								<a href="#">
									<h3 class="h4 title-project text-cap">diaz sunprotection decoration</h3>
								</a>
							</div>
						</div>
					</div> <!-- End project Container -->
				</div> <!-- End  -->
			</section>
			<!-- End Section Isotop Lastest Project -->

			<section class="partners padding bg-grey">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<h2 class="title text-cap">NOS PARTENAIRES</h2>
							<div class="divider divider-1">
								<svg class="svg-triangle-icon-container">
									<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
								</svg>
							</div>
						</div>
						<!-- End Title -->
						<div class="owl-partner-warp">
							<div class="customNavigation">
								<a class="btn prev-partners"><i class="fa fa-angle-left"></i></a>
								<a class="btn next-partners"><i class="fa fa-angle-right"></i></a>
							</div><!-- End owl button -->

							<div id="owl-partners" class="owl-carousel owl-theme owl-partners clearfix">
								<div class="item">
									<a href="#">
										<img src="images/villanova.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/kirkbydesign.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/zinc.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/marcAlexander.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/romo.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/zinc.svg" class="img-responsive" alt="Image">
									</a>
								</div>
								<div class="item">
									<a href="#">
										<img src="images/kirkbydesign.svg" class="img-responsive" alt="Image">
									</a>
								</div>
							</div>
						</div><!-- End row partners -->
					</div>
				</div>
			</section>
			<!-- End Section Owl Partners -->

			<section class="padding newsletter">
				<div class="container">
					<div class="row">
						<div class="title-block">
							<h2 class="title text-cap">nOS DISTRIBUTIONS</h2>
							<div class="divider divider-1">
								<svg class="svg-triangle-icon-container">
									<polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
								</svg>
							</div>
						</div>
						<!-- End Title -->
						<div class="form-subcribe">

							<p class="text-center">Abonnez-vous à notre newsletter pour recevoir nos dernières nouveautés et des offres exclusives</p>
							<form method="post">
								<input class="newsletter-email input-text" placeholder="" type="email">
								<button class="ot-btn btn-main-color text-cap" type="submit">je m'abonne</button>
							</form>
						</div>
					</div>
				</div>
			</section>
			<!-- End Section subcribe -->
			<?php @include("./components/footer.php"); ?>
		</div>
	</div>
	<!-- End page -->

	<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
	<!-- Back To Top -->


	<!-- SCRIPT -->
	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins/jquery.waypoints.min.js"></script>
	<script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/plugins/wow.min.js"></script>
	<script type="text/javascript" src="js/plugins/skrollr.min.js"></script>

	<!-- REVOLUTION JS FILES -->
	<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
	<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>

	<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
			(Load Extensions only on Local File Systems !  
			The following part can be removed on Server for On Demand Loading) -->
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script>
	<!-- Intializing Slider-->
	<script type="text/javascript" src="js/plugins/slider.js"></script>

	<!-- Initializing the isotope
	    ================================================== -->
	<script src="js/plugins/isotope.pkgd.min.js"></script>
	<script src="js/plugins/custom-isotope.js"></script>
	<!-- Initializing Owl Carousel
	    ================================================== -->
	<script src="js/plugins/owl.carousel.js"></script>
	<script src="js/plugins/custom-owl.js"></script>
	<!-- Initializing Counter Up
    ================================================== -->
	<script src="js/plugins/jquery.counterup.min.js"></script>
	<?php @include("./components/footer-script.php"); ?>

</body>

</html>