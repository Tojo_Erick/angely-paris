<header id="mainmenu" class="header-v1 header-border header-fix header-bg-white" data-0="padding:10px;padding-left:40px;padding-right:40px;" data-251="padding:10px; padding-left:40px;padding-right:40px;top:0;">
    <div class="left-header">
        <ul class="navi-level-1">
            <li><a href="/" class="logo"><img src="images/logo.png" class="img-responsive" alt="Image"></a></li>
        </ul>
    </div><!-- End Left Header -->
    <nav>
        <ul class="navi-level-1 hover-style-2 main-navi">
            <li><a href="/"><span>accueil</span></a></li>
            <li><a href="/societe.php"><span>la société</span></a></li>
            <li><a href="/tissus-d-ameublement.php"><span>tissus d'ameublement</span></a></li>
            <li><a href="/mobilier-sur-mesure.php"><span>MOBILIER sur mesure </span></a></li>
            <li><a href="/nos-distributions.php"><span>nos distributions</span></a></li>
            <li><a href="#"><span>catalogue</span></a></li>
            <li><a href="/contact.php"><span>Contact</span></a></li>
            <!-- <li><a href="contact.html"><span>Contact</span></a>
              <ul class="navi-level-2">
                  <li><a href="contact.html">Contact Style 1</a></li>
                  <li><a href="contact_2.html">Contact Style 2</a></li>
                  <li><a href="contact_3.html">Contact Style 3</a></li>                                    
              </ul>
            </li> -->

        </ul>
    </nav><!-- End Nav -->
    <div class="right-header">
        <ul class="navi-level-1 sub-navi seperator-horizonal-line hover-style-4">	
            <li class="header-tel"><a class="tel-header" href="tel:0160569803">01 60 56 98 03</a></li>
            

            <!-- Testing Search Box -->
            <!-- <li><a href="#"><span aria-hidden="true" class="icon_bag_alt"></span>
                </a>
            </li>	 -->
            <li class="header-mail"><a class="tel-header" href="mailto:contact@angely-paris.com">contact@angely-paris.com</a></li>
            
             <li >
                <a  href="#" data-toggle="modal" data-target="#myModal" id="btn-search" class="reset-btn btn-in-navi"><span aria-hidden="true" class="icon_search"></span></a>
            </li>
            <li>
                <a href="#/"  class="mm-toggle">
                    <span aria-hidden="true" class="icon_menu"></span>
                </a> 
            </li>
        </ul>

    </div><!-- End Right Header -->	
</header> 
<!-- End  Header -->