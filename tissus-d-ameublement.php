<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Angely Paris | Tissus d'ameublement</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Font -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/elegant-font.css">
	<link rel="stylesheet" href="css/linearicons.css">
	<!-- SCROLL BAR MOBILE MENU
  		================================================== -->
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
	<!-- Fancy Select
  		================================================== -->

	<link rel="stylesheet" href="css/fancySelect.css" />

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">

	<!-- Favicons
		  ================================================== -->
	<link rel="shortcut icon" href="favicon.png">
</head>

<body class="shop">
	<?php @include("./components/mobile-menu.php"); ?>
	<?php @include("./components/modal-search.php"); ?>
	<!-- End Modal Search-->
	<div id="page">
		<div id="skrollr-body">
			<?php @include("./components/header.php"); ?>
			<section>
				<div class="sub-header sub-header-1 sub-header-about fake-position">
					<div class="sub-header-content">
						<h1 class="text-cap white-text h2">TISSUS D'AMEUBLEMENT</h1>
						<ol class="breadcrumb breadcrumb-arc text-cap">
							<li>
								<a href="/">ACCUEIL</a>
							</li>
							<li class="active">TISSUS D'AMEUBLEMENT</li>
						</ol>
					</div>
				</div>
			</section>
			<!-- End Section Sub Header -->
			<Section class="">
				<div class="container">
					<form class="form-inline form-search-home-6 form-search-shop">
						<div class="form-group">
							<div class="input-group">
								<button type="submit" class="btn-search-home-6 text-cap"><span aria-hidden="true" class="icon_search"></span></button>
								<input class="form-control" id="exampleInputAmount" placeholder="RECHECHER" type="text">
							</div>
						</div>
					</form>
					<div class="blogFilter blog-terms line-effect-2">
						<a href="#" data-filter="*" class="current text-cap">
							<h2>Tout</h2>
						</a>
						<a href="#" data-filter=".unis" class="text-cap">
							<h2>les unis</h2>
						</a>
						<a href="#" data-filter=".contemporains" class="text-cap">
							<h2>les contemporains</h2>
						</a>
						<a href="#" data-filter=".classiques" class="text-cap">
							<h2>les classiques</h2>
						</a>
						<a href="#" data-filter=".florabotanica" class="text-cap">
							<h2>collection florabotanica</h2>
						</a>
						<a href="#" data-filter=".marnay" class="text-cap">
							<h2>collection marnay</h2>
						</a>
						<a href="#" data-filter=".broadway" class="text-cap">
							<h2>collection broadway</h2>
						</a>
					</div> <!-- End Project Fillter -->
					<div class="">
						<div class="main-shop content-area">
							<main id="main" class="site-main padding-top-50">
								<div class="arc-sorting">
									<form method="get" class="woocommerce-ordering">
										<select class="orderby custom-select" name="orderby">
											<option>Trier par</option>
											<option>Tri par popularité</option>
											<option>Tri par note moyenne</option>
											<option>Tri par nouveauté</option>
											<option>Tri par prix : du plus bas au plus élevé</option>
											<option>Tri par prix : du plus élevé au plus bas</option>
										</select>
									</form>
									<p class="woocommerce-result-count">résultats 1 &ndash; 10 sur 23 </p>
								</div> <!-- End Finance Sorting -->
								<div class="row row-products">
									<div class="products">
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-1.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>

												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														romeo
														</h3>
													</a>
													<!-- <p class="price">
														$99.00
													</p> -->
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-2.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>
													<!-- <span class="sale-product">SALE</span> -->
												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
															Renoir
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-3.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
															Panther

														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-4.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														CAVALAIRE


														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-5.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
															Zoé
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-6.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
															Puma
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-7.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														Oxford
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-8.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														VELLUTI
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-9.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														SAMBOURG LARGE
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-10.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														Quincy
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-11.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
															Suit
														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/shop_single.php" class="products-warp">
													<img src="images/Shop/product-12.png" class="img-responsive" alt="Image">
													<span class="new-product">Nouveau</span>

												</a>
												<div class="product-info">
													<a href="/shop_single.php">
														<h3 class="text-cap">
														eragny

														</h3>
													</a>
													<a href="/shop_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
									</div><!-- End Products -->
								</div><!-- End Row -->

								<nav class="woocommerce-pagination padding-top-30">
									<ul class="page-numbers pagination">
										<li><a class="current" href="/tissus-d-ameublement.php">1</a></li>
										<li><a href="/tissus-d-ameublement.php">2</a></li>
										<li><a href="/tissus-d-ameublement.php">3</a></li>
										<li class="threedots">...</li>
										<li><a href="/tissus-d-ameublement.php">6</a></li>
										<li><a href="/tissus-d-ameublement.php"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									</ul>
								</nav>
							</main> <!-- End Main -->
						</div>
					</div><!-- End Row -->
				</div><!-- End container -->
			</Section> <!--End List Product -->
			<!-- End Section subcribe -->
			<?php @include("./components/footer.php"); ?>
		</div>
	</div>
	<!-- End page -->

	<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
	<!-- Back To Top -->
	<!-- SCRIPT -->
	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins/fancySelect.js"></script>
	<script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
	<script src="js/plugins/wow.min.js"></script>

	<?php @include("./components/footer-script.php"); ?>

</body>

</html>