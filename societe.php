<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Angely Paris | Société</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/elegant-font.css">
    <!-- SCROLL BAR MOBILE MENU
  		================================================== -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
    <!-- OWL CAROUSEL
	  	================================================== -->
    <link rel="stylesheet" href="css/owl.carousel.css">

    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Favicons
		  ================================================== -->
    <link rel="shortcut icon" href="favicon.png">
</head>

<body class="la-societe">
    <?php @include("./components/mobile-menu.php"); ?>
    <?php @include("./components/modal-search.php"); ?>
    <!-- End Modal Search-->
    <div id="page">
        <div id="skrollr-body">
            <?php @include("./components/header.php"); ?>
            <section>
                <div class="sub-header sub-header-1 sub-header-about fake-position">
                    <div class="sub-header-content">
                        <h1 class="text-cap white-text h2">La société</h1>
                        <ol class="breadcrumb breadcrumb-arc text-cap">
                            <li>
                                <a href="/">ACCUEIL</a>
                            </li>
                            <li class="active">LA SOCIETE</li>
                        </ol>
                    </div>
                </div>
            </section>
            <!-- End Section Sub Header -->

            <section class="padding societe societe1">
                <div class="container">
                    <div class="row">
                        <div class="wrap-sc">
                            <div class="col-md-5">
                                <div class="block-img-left">
                                    <div class="img-block"><img src="images/la-societe.jfif" class="img-responsive" alt="Image"></div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="block-img-right">
                                    <div class="text-box">
                                        <h2 class="text-cap">Angely PARIS</h2>
                                        <p>
                                            Crée en 1998 par Pierre Richard Calice, la société Angely Paris est éditeur et distributeur de tissus d'ameublement haut de gamme pour la décoration d'intérieure.
                                            <br /><br />La société collabore de manière significative avec les Tapissiers, Décorateurs indépendants et Bureaux d'études pour répondre à leurs besoins en proposant à ces professionnels du secteur une large offre de textiles alliant différents styles et matières grâce notamment à la diversité des marques que nous distribuons.
                                            <br /><br /><b>Une remarque ou une question ? <a href="contact.html" style="color:#666;">Contactez-nous !</a></b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Section What we do -->

            <section class="padding societe societe2">
                <div class="container real-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="block-img-right">
                                <div class="container">
                                    <div class="text-box">
                                        <h2 class="text-cap">Expert Dans<br /> Notre Domaine</h2>
                                        <p>
                                            Angely Paris s'est développée autour de cette activité principale sans négliger l'intégration de superbes collections de papiers peints coordonnées à certaines de nos gammes tissus. Depuis 2010, Angely Paris propose également à ses clients une gamme de meubles sur mesure haut de gamme confectionnés dans le ou les tissu(s) sélectionnés par le client permettant à ce dernier de satisfaire ses envies.
                                        </p>
                                    </div>
                                    <div class="four-blocks">
                                        <div class="blc">
                                            <h3 class="blc-ttl">LUMINAIRES</h3>
                                            <div class="blc-icon"><img src="images/ampoule.png" class="img-responsive" alt="Image"/></div>
                                        </div>
                                        <div class="blc">
                                            <h3 class="blc-ttl">store et Tissus<br /> d’ameublement</h3>
                                            <div class="blc-icon"><img src="images/motif-de-tissu.png" class="img-responsive" alt="Image"/></div>
                                        </div>
                                        <div class="blc">
                                            <h3 class="blc-ttl">Papier peints</h3>
                                            <div class="blc-icon"><img src="images/colle.png" class="img-responsive" alt="Image"/></div>
                                        </div>
                                        <div class="blc">
                                            <h3 class="blc-ttl">Mobilier<br /> sur mesure</h3>
                                            <div class="blc-icon"><img src="images/meubles.png" class="img-responsive" alt="Image"/></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block-img-left">
                                <div class="img-block"><img src="images/fauteuil.jpg" class="img-responsive" alt="Image"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid fake-container">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
            </section>
            <!-- End Section What we do -->

            <section class="partners padding bg-grey">
                <div class="container">
                    <div class="row">
                        <div class="title-block">
                            <h2 class="title text-cap">NOS PARTENAIRES</h2>
                            <div class="divider divider-1">
                                <svg class="svg-triangle-icon-container">
                                    <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                                </svg>
                            </div>
                        </div>
                        <!-- End Title -->
                        <div class="owl-partner-warp">
                            <div class="customNavigation">
                                <a class="btn prev-partners"><i class="fa fa-angle-left"></i></a>
                                <a class="btn next-partners"><i class="fa fa-angle-right"></i></a>
                            </div><!-- End owl button -->

                            <div id="owl-partners" class="owl-carousel owl-theme owl-partners clearfix">
                                <div class="item">
                                    <a href="#">
                                        <img src="images/villanova.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/kirkbydesign.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/zinc.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/marcAlexander.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/romo.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/zinc.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="#">
                                        <img src="images/kirkbydesign.svg" class="img-responsive" alt="Image">
                                    </a>
                                </div>
                            </div>
                        </div><!-- End row partners -->
                    </div>
                </div>
            </section>
            <!-- End Section Owl Partners -->

            <section class="padding newsletter">
                <div class="container">
                    <div class="row">
                        <div class="title-block">
                            <h2 class="title text-cap">nOS DISTRIBUTIONS</h2>
                            <div class="divider divider-1">
                                <svg class="svg-triangle-icon-container">
                                    <polygon class="svg-triangle-icon" points="6 11,12 0,0 0"></polygon>
                                </svg>
                            </div>
                        </div>
                        <!-- End Title -->
                        <div class="form-subcribe">

                            <p class="text-center">Abonnez-vous à notre newsletter pour recevoir nos dernières nouveautés et des offres exclusives</p>
                            <form method="post">
                                <input class="newsletter-email input-text" placeholder="" type="email">
                                <button class="ot-btn btn-main-color text-cap" type="submit">je m'abonne</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Section subcribe -->
            <?php @include("./components/footer.php"); ?>
        </div>
    </div>
    <!-- End page -->

    <a id="to-the-top"><i class="fa fa-angle-up"></i></a>
    <!-- Back To Top -->
    <!-- SCRIPT -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
    <script src="js/plugins/wow.min.js"></script>

    <!-- Initializing Owl Carousel
    ================================================== -->
    <script src="js/plugins/owl.carousel.js"></script>
    <script src="js/plugins/custom-owl.js"></script>
    <?php @include("./components/footer-script.php"); ?>

</body>

</html>