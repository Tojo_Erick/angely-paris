<!DOCTYPE html>
<html lang="">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Angely Paris | Mobilier sur mesure</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- Font -->
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/elegant-font.css">
	<link rel="stylesheet" href="css/linearicons.css">
	<!-- SCROLL BAR MOBILE MENU
  		================================================== -->
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
	<!-- Fancy Select
  		================================================== -->

	<link rel="stylesheet" href="css/fancySelect.css" />

	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">

	<!-- Favicons
		  ================================================== -->
	<link rel="shortcut icon" href="favicon.png">
</head>

<body class="shop sur-mesure">
	<?php @include("./components/mobile-menu.php"); ?>
	<?php @include("./components/modal-search.php"); ?>
	<!-- End Modal Search-->
	<div id="page">
		<div id="skrollr-body">
			<?php @include("./components/header.php"); ?>
			<section>
				<div class="sub-header sub-header-1 sub-header-about fake-position">
					<div class="sub-header-content">
						<h1 class="text-cap white-text h2">Mobilier sur mesure</h1>
						<ol class="breadcrumb breadcrumb-arc text-cap">
							<li>
								<a href="/">ACCUEIL</a>
							</li>
							<li class="active">Mobilier sur mesure</li>
						</ol>
					</div>
				</div>
			</section>
			<!-- End Section Sub Header -->
			<Section class="">
				<div class="container">
					<form class="form-inline form-search-home-6 form-search-shop">
						<div class="form-group">
							<div class="input-group">
								<button type="submit" class="btn-search-home-6 text-cap"><span aria-hidden="true" class="icon_search"></span></button>
								<input class="form-control" id="exampleInputAmount" placeholder="RECHECHER" type="text">
							</div>
						</div>
					</form>
					<div class="blogFilter blog-terms line-effect-2">
						<a href="#" data-filter="*" class="current text-cap">
							<h2>Tout</h2>
						</a>
						<a href="#" data-filter=".unis" class="text-cap">
							<h2>Canapés</h2>
						</a>
						<a href="#" data-filter=".contemporains" class="text-cap">
							<h2>Fauteuils, chaises, poufs</h2>
						</a>
						<a href="#" data-filter=".classiques" class="text-cap">
							<h2>Tête de lit et sommiers</h2>
						</a>
                        <div class="right-align"><a href="/catalogue.php" class="ot-btn btn-main-color text-cap">Voir le catalogue</a></div>
					</div> <!-- End Project Fillter -->
					<div class="">
						<div class="main-shop content-area">
							<main id="main" class="site-main padding-top-50">
								<div class="arc-sorting">
									<form method="get" class="woocommerce-ordering">
										<select class="orderby custom-select" name="orderby">
											<option>Trier par</option>
											<option>Tri par popularité</option>
											<option>Tri par note moyenne</option>
											<option>Tri par nouveauté</option>
											<option>Tri par prix : du plus bas au plus élevé</option>
											<option>Tri par prix : du plus élevé au plus bas</option>
										</select>
									</form>
									<p class="woocommerce-result-count">résultats 1 &ndash; 10 sur 23 </p>
								</div> <!-- End Finance Sorting -->
								<div class="row row-products">
									<div class="products">
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-1.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>

												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														Canapé AFRICA
														</h3>
													</a>
													<!-- <p class="price">
														$99.00
													</p> -->
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-2.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->
													<!-- <span class="sale-product">SALE</span> -->
												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
                                                        Canapé COLONIA
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-3.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
                                                        Canapé CHILE

														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-4.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														Canapé BOLONIA


														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-5.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
                                                        Canapé LIMA
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-6.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
                                                        Canapé MONTEVIDEO
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-7.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														Canapé SOL
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-8.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														FAUTEUIL CHICAGO
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-9.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														banquette luna
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-10.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														FAUTEUIL AFRICA
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-11.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
                                                        FAUTEUIL Alma
														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="product-item">
												<a href="/canape_single.php" class="products-warp">
													<img src="images/Shop/canape-12.jfif" class="img-responsive" alt="Image">
													<!-- <span class="new-product">Nouveau</span> -->

												</a>
												<div class="product-info">
													<a href="/canape_single.php">
														<h3 class="text-cap">
														FAUTEUIL ANGELA

														</h3>
													</a>
													<a href="/canape_single.php" class="ot-btn btn-block btn-main-color">EN SAVOIR PLUS</a>
												</div>
											</div>
										</div>
									</div><!-- End Products -->
								</div><!-- End Row -->

								<nav class="woocommerce-pagination padding-top-30">
									<ul class="page-numbers pagination">
										<li><a class="current" href="/mobilier-sur-mesure.php">1</a></li>
										<li><a href="/mobilier-sur-mesure.php">2</a></li>
										<li><a href="/mobilier-sur-mesure.php">3</a></li>
										<li class="threedots">...</li>
										<li><a href="/mobilier-sur-mesure.php">6</a></li>
										<li><a href="/mobilier-sur-mesure.php"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
									</ul>
								</nav>
							</main> <!-- End Main -->
						</div>
					</div><!-- End Row -->
				</div><!-- End container -->
			</Section> <!--End List Product -->
			<!-- End Section subcribe -->
			<?php @include("./components/footer.php"); ?>
		</div>
	</div>
	<!-- End page -->

	<a id="to-the-top"><i class="fa fa-angle-up"></i></a>
	<!-- Back To Top -->
	<!-- SCRIPT -->
	<script src="js/vendor/jquery.min.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/plugins/fancySelect.js"></script>
	<script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>
	<script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
	<script src="js/plugins/wow.min.js"></script>

	<?php @include("./components/footer-script.php"); ?>

</body>

</html>