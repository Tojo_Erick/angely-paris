<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Angely Paris | Nos distributions</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/elegant-font.css">
    <!-- SCROLL BAR MOBILE MENU
  		================================================== -->
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.css" />
    <!-- Main Style -->
    <link rel="stylesheet" href="style.css">

    <!-- Favicons
		  ================================================== -->
    <link rel="shortcut icon" href="favicon.png">
</head>

<body class="nos-distributions">
    <?php @include("./components/mobile-menu.php"); ?>
    <?php @include("./components/modal-search.php"); ?>
    <!-- End Modal Search-->
    <div id="page">
        <div id="skrollr-body">
            <?php @include("./components/header.php"); ?>
            <section>
                <div class="sub-header sub-header-1 sub-header-blog-grid fake-position">
                    <div class="sub-header-content">
                        <h1 class="text-cap white-text h2">NOS DISTRIBUTIONS</h1>
                            <ol class="breadcrumb breadcrumb-arc text-cap">
                                <li>
                                    <a href="/">ACCUEIL</a>
                                </li>
                                <li class="active">NOS DISTRIBUTIONS</li>
                            </ol>
                    </div>
                </div>
            </section>
            <!-- End Section Sub Header -->
            <section class="padding-bot-90">
                <div class="container">
                    <div class="row">
                        <div class="blog-warp blog-grid-3-col clearfix">
                            <form class="form-inline form-search-home-6 form-search-shop">
                                <div class="form-group">
                                    <div class="input-group">
                                        <button type="submit" class="btn-search-home-6 text-cap"><span aria-hidden="true" class="icon_search"></span></button>
                                        <input class="form-control" id="exampleInputAmount" placeholder="RECHECHER" type="text">
                                    </div>
                                </div>
                            </form>
                            <div class="clearfix blogContainer blog-grid-3-col-container">

                                <div class="element-item  interior">

                                    <article>
                                        <figure class="latest-blog-post-img">
                                            <a href="#">
                                                <img src="/images/Nos_distributions/1.jfif" class="img-responsive" alt="Image">
                                            </a>

                                        </figure>
                                        <div class="latest-blog-post-description">
                                            <a href="#">
                                                <h2 class="blog-title">ROMO</h2>
                                            </a>
                                            <!-- <div class="latest-blog-post-data">
                                                <p class="tags text-cap">
                                                    <a href="#">Interior Design </a>,
                                                    <a href="#">Art Decor </a>
                                                </p>
                                            </div>
                                            <div class="latest-blog-post-date-2  text-cap">
                                                <span class="month">May</span>
                                                <span class="day">21,</span>
                                                <span class="year">2016</span>
                                            </div> -->
                                            <p class="blog-description">Des générations d'expérience et un design britannique distinctif sont le fondement de la marque Romo. Réputée pour sa bibliothèque variée de designs contemporains et larges palettes de couleurs. Romo est la marque fondatrice du Groupe Romo, comprenant six marques d'intérieur établies, chacune avec son propre caractère et style.</p>
                                            <a href="#" class="site-redir">Allez sur le site</a>
                                        </div>
                                    </article>
                                </div>

                                <div class="element-item interior ">
                                    <article>
                                        <figure class="latest-blog-post-img">
                                            <a href="#">
                                                <img src="images/Nos_distributions/2.jfif" class="img-responsive" alt="Image">
                                            </a>

                                        </figure>
                                        <div class="latest-blog-post-description">
                                            <a href="#">
                                                <h2 class="blog-title">iNOPSIS</h2>
                                            </a>

                                            <p class="blog-description">Villa Nova est une marque du groupe ROMO ayant pour but de créer des tissus, revêtements muraux et accessoires contemporains dans une palette de couleurs tendance à des prix accessibles.</p>
                                            <a href="#" class="site-redir">Allez sur le site</a>
                                        </div>
                                    </article>
                                </div>

                                <div class="element-item Furniture">
                                    <article>
                                        <figure class="latest-blog-post-img">
                                            <a href="#">
                                                <img src="images/Nos_distributions/3.jfif" class="img-responsive" alt="Image">
                                            </a>

                                        </figure>
                                        <div class="latest-blog-post-description">
                                            <a href="#">
                                                <h2 class="blog-title">Fibre Naturelle</h2>
                                            </a>

                                            <p class="blog-description">Fibre Naturelle est une société textile basée en Angleterre depuis 1995, broderies, imprimés, tissages jacquards, motifs floraux, art deco, géométriques, matières velours, toiles ... composent la large gamme de la marque,qui jouit en plus d'excellents niveaux de stock.</p>
                                            <a href="#" class="site-redir">Allez sur le site</a>
                                        </div>
                                    </article>
                                </div>

                                <div class="element-item Furniture ">
                                    <article>
                                        <figure class="latest-blog-post-img">
                                            <a href="#">
                                                <img src="images/Nos_distributions/4.jfif" class="img-responsive" alt="Image">
                                            </a>

                                        </figure>
                                        <div class="latest-blog-post-description">
                                            <a href="#">
                                                <h2 class="blog-title">diaz sunprotection decoration</h2>
                                            </a>
                                            <p class="blog-description">Stores - 100 % fabriqués en Belgique</p>
                                            <a href="#" class="site-redir">Allez sur le site</a>
                                        </div>
                                    </article>
                                </div>

                                <div class="element-item Ecommercial">
                                    <article>
                                        <figure class="latest-blog-post-img">
                                            <a href="#">
                                                <img src="images/Nos_distributions/5.jfif" class="img-responsive" alt="Image">
                                            </a>

                                        </figure>
                                        <div class="latest-blog-post-description">
                                            <a href="#">
                                                <h2 class="blog-title">Heathfield & Co</h2>
                                            </a>
                                            <p class="blog-description">Luminaires décoratifs</p>
                                            <a href="#" class="site-redir">Allez sur le site</a>
                                        </div>
                                    </article>
                                </div>
                            </div> <!-- End project Container -->
                        </div> <!-- End  -->
                    </div>
                </div>
            </section>
            <!-- End Section Isotop Lastest Project -->
            <?php @include("./components/footer.php"); ?>
        </div>
    </div>
    <!-- End page -->

    <a id="to-the-top"><i class="fa fa-angle-up"></i></a>
    <!-- Back To Top -->
    <!-- SCRIPT -->
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins/wow.min.js"></script>
    <script type="text/javascript" src="js/plugins/skrollr.min.js"></script>
    <script src="js/plugins/jquery.mCustomScrollbar.concat.min.js"></script>

    <!-- Initializing the isotope
    ================================================== -->
    <script src="js/plugins/isotope.pkgd.min.js"></script>
    <script src="js/plugins/custom-isotope.js"></script>

    <?php @include("./components/footer-script.php"); ?>
</body>

</html>